import { Router } from 'express';
import * as routers from './routers';

export class AppRouter {
  protected router: Router;

  constructor() {
    this.router = Router();
  }

  /**
   * Main app router
   */
  public getAppRouter(): Router {
    // players routes
    this.router.use('/', routers.playersRouter.router);
    return this.router;
  }
}
