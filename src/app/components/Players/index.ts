import { PlayersController } from './Players.controller';
import * as playersValidator from './Players.validators';
export { PlayersController, playersValidator };
