import * as debug from 'debug';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import * as service from '../../services/api';
import { Player } from '../../services/api.types';
const logger = debug('app:src/app/components/Players/Players.controller.ts');

/**
 * Example `Players` controller
 */
export class PlayersController {
  /**
   * API players endpoint
   * GET /players
   */
  public getPlayers = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const players: Player[] = await service.getPlayers();
      res.status(200).json(players);
    } catch (err) {
      logger('endpointGetHealthCheck:: error: ', err);
      next(err);
    }
  };

  /**
   * GET player by id endpoint
   * GET /players/:id
   */
  public getPlayerById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { id },
      } = req;
      const player: Player = await service.getPlayerById(id);
      res.status(200).json(player);
    } catch (err) {
      logger('endpointPostDisplayName:: error: ', err);
      next(err);
    }
  };
}
