import * as Joi from 'joi';

export const getPlayerById = Joi.object({
  id: Joi.string().required(),
});
