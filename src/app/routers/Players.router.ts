import { Router } from 'express';
import * as validation from 'express-joi-validation';
import { PlayersController, playersValidator } from '../components/Players';

export class PlayersRouter {
  public router: Router;
  protected playersController: PlayersController;
  protected validator: any;

  constructor() {
    this.playersController = new PlayersController();
    this.validator = validation({ passError: true });
    this.router = this.initRouter();
  }

  /**
   * Players router
   */
  private initRouter(): Router {
    const router: Router = Router();

    router
      .get('/players', this.playersController.getPlayers)
      .get(
        '/players/:id',
        this.validator.params(playersValidator.getPlayerById),
        this.playersController.getPlayerById
      );

    return router;
  }
}

export const playersRouter = new PlayersRouter();
