import fetch from 'node-fetch';
import { Player } from './api.types';
import { find } from 'lodash';

export const endpoint: string =
  'https://eurosportdigital.github.io/eurosport-node-developer-recruitment/headtohead.json';

export const getPlayers = async (): Promise<Player[]> => {
  const res = await fetch(endpoint);
  const data = await res.json();
  return data.players;
};

export const getPlayerById = async (id: string): Promise<Player> => {
  const players: Player[] = await getPlayers();
  const player: Player | undefined = find(
    players,
    (player: Player): boolean => player.id.toString() === id
  );
  if (!player) throw new Error('no player found');
  return player;
};
